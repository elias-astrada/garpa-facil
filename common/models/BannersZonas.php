<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "banners_zonas".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $activo
 *
 * @property BannersXZonas[] $bannersXZonas
 * @property Banners[] $banners
 */
class BannersZonas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banners_zonas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'activo'], 'required'],
            [['activo'], 'integer'],
            [['nombre'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannersXZonas()
    {
        return $this->hasMany(BannersXZonas::className(), ['zonas_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanners()
    {
        return $this->hasMany(Banners::className(), ['id' => 'banners_id'])->viaTable('banners_x_zonas', ['zonas_id' => 'id']);
    }
    
    
    
    
    public static function renderBanner($nombre){
        $banner_zona = self::find()->where(['nombre' => $nombre])->one();
        
        if(is_null($banner_zona)){
            Yii::error("Error cargando banner");
            return '';
        }
        
        $banners = $banner_zona->banners;

        $rand = rand ( 0 , count($banners) -1 );
        $i = 0; foreach($banners as $banner){
            if($i == $rand) return $banner->render();
            $i++;
        }

    }
    
    
}
