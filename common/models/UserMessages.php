<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_messages".
 *
 * @property integer $id
 * @property integer $user_from
 * @property integer $user_to
 * @property string $message
 * @property integer $fecha
 * @property integer $visto
 *
 * @property User $userFrom
 * @property User $userTo
 */
class UserMessages extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_messages';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['user_from', 'user_to', 'message', 'fecha', 'visto'], 'required'],
                [['user_from', 'user_to', 'fecha', 'visto'], 'integer'],
                [['message'], 'string'],
                [['user_from'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_from' => 'id']],
                [['user_to'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_to' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_from' => 'Remitente',
            'user_to' => 'Destinatario',
            'message' => 'Mensaje',
            'fecha' => 'Fecha',
            'visto' => 'Visto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserFrom() {
        return $this->hasOne(User::className(), ['id' => 'user_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTo() {
        return $this->hasOne(User::className(), ['id' => 'user_to']);
    }
    
    public function isVisto(){
        return $this->visto;
    }
    
    public function setVisto(){
        $this->visto = 1;
        $this->save(false);
    }

    public static function getMensajes() {
        return UserMessages::find()->where(['user_to' => Yii::$app->user->id , 'visto' => 0]);
    }

}
