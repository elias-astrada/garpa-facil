<?php

namespace common\widgets;

use Yii;
use yii\widgets\InputWidget;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * Yii::$app->session->setFlash('error', 'This is the message');
 * Yii::$app->session->setFlash('success', 'This is the message');
 * Yii::$app->session->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 */
class GaleriaInput extends InputWidget {

    public $inputname = "";
    public $previewUrl = "";
    public $multiple = false;

    /**

      /**
     * Initializes the widget.
     * If you override this method, make sure you call the parent implementation first.
     */
    public function init() {
        //parent::init();
        //Yii::error($this->hasModel());
    }

    /**
     * @inheritdoc
     */
    public function run() {
        parent::run();

        \backend\assets\ImagenesAsset::register($this->view);


        if ($this->multiple) {
            echo $this->renderInputMultiple();
        } else {
            echo $this->renderInput();
        }

        $this->initJavascript();
    }

    
    
    
       /**
     * Renders the source Input for the Switch plugin. Graceful fallback to a normal HTML checkbox or radio input in
     * case JQuery is not supported by the browser
     *
     * @return string
     */
    protected function renderInputMultiple() {




        $input = Html::activeInput('hidden', $this->model, $this->attribute, ['class' => 'imgpanel-' . $this->attribute]);
        $this->inputname = 'imgpanel-' . $this->attribute.'[]';

        $modals = <<<HTML

            <div class="modal fade modal-scroll" id="modal-subir" tabindex="-1" role="dialog" aria-hidden="true">      
                <div class="modal-dialog">
                    <div class="modal-content">
                    </div>
                </div>
            </div>


            <div class="modal fade modal-scroll" id="modal-recortes" tabindex="-1" role="dialog" aria-hidden="true">      
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                    </div>
                </div>
            </div>

HTML;



        return $input .
                '<div class="img-panel-widget" style="max-width:20rem">' .
                '<img src="' . $this->getPreviewUrl() . '"class="img-panel-widget-preview img-rounded img-fluid" >' .
                '<a href="javascript:void(0)" id="btn-img-selector-panel" class="btn btn-block btn-primary"  data-toggle="slidePanel" data-url="' . Url::toRoute(['/imagenes/input', 'class' => 'imgpanel-container-' . $this->attribute]) . '" >'
                . 'Elegir imagen</a>'
                . $modals .
                '</div>';
    }
    
    
    
    /**
     * Renders the source Input for the Switch plugin. Graceful fallback to a normal HTML checkbox or radio input in
     * case JQuery is not supported by the browser
     *
     * @return string
     */
    protected function renderInput() {




        $input = Html::activeInput('hidden', $this->model, $this->attribute, ['class' => 'imgpanel-' . $this->attribute]);
        $this->inputname = 'imgpanel-' . $this->attribute;

        $modals = <<<HTML

            <div class="modal fade modal-scroll" id="modal-subir" tabindex="-1" role="dialog" aria-hidden="true">      
                <div class="modal-dialog">
                    <div class="modal-content">
                    </div>
                </div>
            </div>


            <div class="modal fade modal-scroll" id="modal-recortes" tabindex="-1" role="dialog" aria-hidden="true">      
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                    </div>
                </div>
            </div>

HTML;



        return $input .
                '<div class="img-panel-widget" style="max-width:20rem">' .
                '<img src="' . $this->getPreviewUrl() . '"class="img-panel-widget-preview img-rounded img-fluid" >' .
                '<a href="javascript:void(0)" id="btn-img-selector-panel" class="btn btn-block btn-primary"  data-toggle="slidePanel" data-url="' . Url::toRoute(['/imagenes/input', 'class' => 'imgpanel-container-' . $this->attribute]) . '" >'
                . 'Elegir imagen</a>'
                . $modals .
                '</div>';
    }

    protected function getPreviewUrl() {
        if (!empty($this->previewUrl))
            return $this->previewUrl;
        else
            return Yii::$app->urlManagerFront->createUrl(['/uploads/nothumb.png']);
    }

    protected function initJavascript() {

        $this->view->registerJs(
                <<<JAVASCRIPT

            $(".imgpanel-container-$this->attribute #modal-subir").bind("img-uploaded-event",function(e,data){
                

                
                var img_id = data.response.uploadedid; 
                var img_url = data.response.uploadedurl;
                

                
                
                $('.$this->inputname').val(img_id);
                $('.img-panel-widget-preview').attr("src",img_url);
             });

           $(document).on('click', '.imgpanel-container-$this->attribute .img-input-select', function(e){ 
                var img_id = $(this).data("imgid"); 
                var img_url = $(this).data("imgurl");

                
                $('.$this->inputname').val(img_id);
                $('.img-panel-widget-preview').attr("src",img_url);
                


                $('.img-panel-widget-preview').trigger("click");

           });

JAVASCRIPT
        );
    }

    /**
     * @return bool whether this widget is associated with a data model.
     */
    protected function hasModel() {
        return $this->model instanceof Model && $this->attribute !== null;
    }

}
