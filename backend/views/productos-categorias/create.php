<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductosCategorias */

$this->title = 'Crear Productos Categorias';
$this->params['breadcrumbs'][] = ['label' => 'Productos Categorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="productos-categorias-crear">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>


</div>