<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UserMessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mensajes';
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['page-header-show'] = false;
Yii::$app->params['page-content-show'] = false;
Yii::$app->params['page-body-class'] = 'app-mailbox page-aside-left site-menubar-unfold';


                
//INDAGAR EN ESTOS JS POR QUE ESTA EL CODIGO DEL SLIDEPANEL AJAX , CARGA EL CONTENIDO EN MEDIA.JS                
$this->registerJsFile(Url::toRoute('template/assets/js/BaseApp.js'),['depends' => [\yii\web\JqueryAsset::className(), backend\assets\TemplateAsset::className()]]);    

$this->registerJsFile(Url::toRoute('template/assets/js/App/Media.js'),['depends' => [\yii\web\JqueryAsset::className(), backend\assets\TemplateAsset::className()]]);

$this->registerJsFile(Url::toRoute('template/assets/examples/js/apps/media.js'), ['depends' => [\yii\web\JqueryAsset::className() , backend\assets\TemplateAsset::className() ]]);





?>



<!-- Mailbox Sidebar -->
<div class="page-aside">
    <div class="page-aside-switch">
        <i class="icon md-chevron-left" aria-hidden="true"></i>
        <i class="icon md-chevron-right" aria-hidden="true"></i>
    </div>
    <div class="page-aside-inner page-aside-scroll">
        <div data-role="container">
            <div data-role="content">
                <div class="page-aside-section">
                    <div class="list-group">
                        <a class="list-group-item list-group-item-action <?= $send_active ? '' : 'active' ?>" href="<?= Url::toRoute(['/user-messages/index']) ?>">
                            <span class="badge badge-pill badge-danger"><?= $inbox_n ?></span>
                            <i class="icon md-inbox" aria-hidden="true"></i> Bandeja de entrada
                        </a>
                        <a class="list-group-item <?= $send_active ? 'active' : '' ?>" href="<?= Url::toRoute(['/user-messages/send']) ?>">
                            <i class="icon md-email" aria-hidden="true"></i> Enviados
                        </a>


                        <a href="javascript:void(0)" id="btn-img-selector-panel" class="panel-galeria-activate "  data-toggle="slidePanel" data-url="<?= Url::toRoute(['/imagenes/input', 'class' => 'noticias-editor-insertimg']) ?>" >dgdfg</a>

                        <?= Html::a('<i class="icon md-delete"></i> Borrar Seleccionados', 'javascript:void(0)', ['id' => 'btn-bulk-del', 'class' => 'list-group-item', 'title' => 'Borrar Seleccionados']); ?>
                    </div>
                </div>
                <div class="page-aside-section">
                    <div class="list-group">
                        <a class="list-group-item" href="<?= Url::toRoute(['/user-messages/create']); ?>">
                            <i class="icon md-edit" aria-hidden="true"></i> Nuevo Mensaje
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Mailbox Content -->
<div class="page-main">
    <!-- Mailbox Header -->
    <div class="page-header">
        <h1 class="page-title"><?= Html::encode($this->title) ?></h1>
        <div class="page-header-actions">
            <!-- <form>
                 <div class="input-search input-search-dark">
                     <i class="input-search-icon md-search" aria-hidden="true"></i>
                     <input type="text" class="form-control" name="" placeholder="Buscar...">
                 </div>
             </form>-->
        </div>
    </div>
    <!-- Mailbox Content -->
    <div id="mailContent" class="page-content page-content-table" data-plugin="selectable">
        <!-- Actions -->


        <?php
        $toolbar = Html::a('<i class="glyphicon glyphicon-plus"></i> Nuevo Mensaje ', ['create'], ['class' => 'btn btn-success']) .
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['class' => 'btn btn-default', 'title' => 'Actualizar']);
        $layout = <<< HTML

    <div class="clearfix"></div>
    {items}
    {pager}
HTML;
        ?>

        <?=
        GridView::widget([
            'export' => [
                'target' => GridView::TARGET_SELF,
                'label' => 'Exportar',
            ],
            'exportConfig' => Yii::$app->params['gridview_exportConfig'],
            'dataProvider' => $dataProvider,
            'toolbar' => [
                    ['content' => $toolbar],
                // '{export}',
                '{toggleData}',
            ],
            'bordered' => false,
            'striped' => true,
            'condensed' => false,
            'pjax' => true,
            'layout' => $layout,
            'panel' => false,
            'containerOptions' => ['class' => 'grid-pjax-container'],
            'options' => ['id' => 'grid-pjax'],
            'rowOptions' => function($model) {

                return ['class' => 'danger', 'data-url' => Url::toRoute(['/user-messages/view', 'id' => $model->id]), 'data-toggle' => 'slidePanel'];
            },
            //'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'kartik\grid\CheckboxColumn'],
                //'id',
                //'user_to',
                [
                    'label' => false,
                    'value' => function($model) {
                        return Html::a($model->username, ['/user/profile', 'id' => $model->user_to]) . '<br>' . \yii\helpers\StringHelper::truncateWords($model->message, 20, '...');
                    },
                    'format' => 'raw',
                ],
                    [
                    'attribute' => 'fecha',
                    'value' => function($model) {
                        return \common\utils\Utils::time_elapsed_string_timestamp($model->fecha);
                    },
                    'width' => '25%',
                    'label' => false,
                ],
                // 'visto',
                /*[
                    'class' => 'kartik\grid\ActionColumn',
                    'template' => '{view}{delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-primary btn-action remote-modal', 'target' => '_BLANK', 'data-target' => '#ajax', 'data-toggle' => 'modal']);
                        },
                    ],
                    'updateOptions' => [
                        'class' => 'btn btn-primary btn-action',
                    ],
                    'deleteOptions' => [
                        'class' => 'btn btn-danger btn-action',
                    ],
                    'width' => '10px',
                    'noWrap' => true,
                ],*/
            ],
        ]);
        ?>


    </div>
</div>
<div class="site-action" data-plugin="actionBtn">
    <button type="button" data-action="add" class="site-action-toggle btn-raised btn btn-success btn-floating" data-toggle ="slidePanel" data-url= "<?= Url::toRoute(['/user-messages/create']); ?>">
        <i class="front-icon md-edit animation-scale-up" aria-hidden="true"></i>
        <i class="back-icon md-close animation-scale-up" aria-hidden="true"></i>
    </button>
    <div class="site-action-buttons">
        <button type="button" data-action="trash" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
            <i class="icon md-delete" aria-hidden="true"></i>
        </button>
        <button type="button" data-action="inbox" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
            <i class="icon md-inbox" aria-hidden="true"></i>
        </button>
    </div>
</div>

<!-- Create New Messages Modal -->






<div class="modal fade modal-scroll" id="ajax" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>



<?= $this->registerJs(' 
    $(document).ready(function(){
      $("#btn-bulk-del").click(function(){
        $.post(
            "delete-multiple", 
            {
                pk : $("#grid-pjax").yiiGridView("getSelectedRows")
            },
            function () {
                $.pjax.reload({container:"#grid-pjax"});
            }
        );
      });
    });', \yii\web\View::POS_READY);
?>