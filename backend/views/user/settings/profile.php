<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use dektrium\user\helpers\Timezone;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\FileInput;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $model
 */
$this->title = Yii::t('user', 'Profile settings');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile(Url::toRoute('template/assets/examples/css/pages/profile.css'));
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>


<div class="row">
    <?= $this->render('/profile_sidebar', ['profile' => $profile]) ?>



    <div class="col-lg-9">

        <!-- END PROFILE CONTENT -->        <!-- Panel -->
        <div class="panel">

            <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                    <li class="nav-item" role="presentation"><a class="active nav-link" data-toggle="tab" href="#activities"
                                                                aria-controls="activities" role="tab">Información Personal </a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#profile" aria-controls="profile"
                                                                role="tab">Cambiar Avatar</a></li>

                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active animation-slide-left" id="activities" role="tabpanel" style="    padding-top: 1.5rem;">
                        <?php
                        $form = ActiveForm::begin([
                                    'id' => 'profile-form',
                                    'options' => ['class' => 'form-horizontal'],
                                    'fieldConfig' => [
                                        'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
                                        'labelOptions' => ['class' => 'col-lg-3 control-label'],
                                    ],
                                    'enableAjaxValidation' => true,
                                    'enableClientValidation' => false,
                                    'validateOnBlur' => false,
                        ]);
                        ?>

                        <?= $form->field($model, 'name') ?>

                        <?= $form->field($model, 'public_email') ?>

                        <?= $form->field($model, 'website') ?>

                        <?= $form->field($model, 'location') ?>

                        <?= $form->field($model, 'timezone')->dropDownList(ArrayHelper::map(Timezone::getAll(), 'timezone', 'name')); ?>


                        <?= $form->field($model, 'bio')->textarea() ?>

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-9">
                                <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?>
                                <br>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="tab-pane animation-slide-left" id="profile" role="tabpanel">
                        <?=
                        FileInput::widget([
                            'name' => 'profile-avatar',
                            'options' => [
                                'multiple' => true
                            ],
                            'pluginOptions' => [
                                'uploadUrl' => Url::to(['/user/settings/avatar-upload', 'id' => $model->user_id]),
                                'maxFileCount' => 1
                            ]
                        ]);
                        ?>
                    </div>

                </div>
            </div>

        </div>
        <!-- End Panel -->
    </div>

</div>